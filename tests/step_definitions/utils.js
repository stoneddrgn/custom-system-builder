let actorIds = {};

const resetActorIds = () => {
    actorIds = {};
};

const setActorId = (key, id) => {
    actorIds[key] = {
        ...actorIds[key],
        id: id
    };

    return id;
};

const setActorTemplateId = (key, id) => {
    actorIds[key] = {
        ...actorIds[key],
        templateId: id
    };

    return id;
};

const getActorId = (key) => {
    return actorIds[key]?.id;
};

const getActorTemplateId = (key) => {
    return actorIds[key]?.templateId;
};

module.exports = {
    resetActorIds,
    setActorId,
    setActorTemplateId,
    getActorId,
    getActorTemplateId
};

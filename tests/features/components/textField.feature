Feature: Text field configuration

  Background:
    Given I setup the Foundry Instance
    And I create a actor template named 'AutoTest_Template'

  Scenario: Basic text field creation
    When I add a component to the 'custom_body' component in actor template 'AutoTest_Template'
    And I choose 'Text field' as component type
    And I type 'textfield_key' as component key
    And I type 'Text field tooltip' as component tooltip
    And I click 'Save Component'

    When I create a character named 'AutoTest_Character'
    And I assign the 'AutoTest_Template' template to the 'AutoTest_Character' character

    Then the character 'AutoTest_Character' looks like 'textField/BasicTextField'

  Scenario: Full text field creation
    When I add a component to the 'custom_body' component in actor template 'AutoTest_Template'
    And I choose 'Text field' as component type
    And I type 'textfield_key' as component key
    And I type 'Text field tooltip' as component tooltip
    And I type 'Text field label' as text field label
    And I type 'azerty' as text field allowed character list
    And I type '5' as text field maximum length
    And I type 'raet' as text field default value
    And I click 'Save Component'

    When I create a character named 'AutoTest_Character'
    And I assign the 'AutoTest_Template' template to the 'AutoTest_Character' character

    Then the character 'AutoTest_Character' looks like 'textField/FullTextField'

  Scenario: Allowed character list checking
    When I add a component to the 'custom_body' component in actor template 'AutoTest_Template'
    And I choose 'Text field' as component type
    And I type 'textfield_key' as component key
    And I type 'azerty' as text field allowed character list
    And I click 'Save Component'

    When I create a character named 'AutoTest_Character'
    And I assign the 'AutoTest_Template' template to the 'AutoTest_Character' character
    And I type 'rarety' in 'textfield_key' in the 'AutoTest_Character' character
    Then the field 'textfield_key' of the character 'AutoTest_Character' has value 'rarety'
    When I type 'not allowed' in 'textfield_key' in the 'AutoTest_Character' character
    Then a 'warning' notification is displayed with text 'Value contains unauthorized characters'
    And the field 'textfield_key' of the character 'AutoTest_Character' has value 'rarety'

  Scenario: Maximum length checking
    When I add a component to the 'custom_body' component in actor template 'AutoTest_Template'
    And I choose 'Text field' as component type
    And I type 'textfield_key' as component key
    And I type '5' as text field maximum length
    And I click 'Save Component'

    When I create a character named 'AutoTest_Character'
    And I assign the 'AutoTest_Template' template to the 'AutoTest_Character' character
    And I type 'small' in 'textfield_key' in the 'AutoTest_Character' character
    Then the field 'textfield_key' of the character 'AutoTest_Character' has value 'small'
    When I type 'loooong' in 'textfield_key' in the 'AutoTest_Character' character
    Then the field 'textfield_key' of the character 'AutoTest_Character' has value 'loooo'

  Scenario: Default value checking
    When I add a component to the 'custom_body' component in actor template 'AutoTest_Template'
    And I choose 'Text field' as component type
    And I type 'textfield_key' as component key
    And I type 'VaLuE' as text field default value
    And I click 'Save Component'

    When I create a character named 'AutoTest_Character'
    And I assign the 'AutoTest_Template' template to the 'AutoTest_Character' character
    Then the field 'textfield_key' of the character 'AutoTest_Character' has value 'VaLuE'

#TODO Size testing
#TODO Advanced configuration